﻿using System;
using System.Threading;
using Application.Abstract.Repositories;
using System.Threading.Tasks;
using Domain.Entities.RegisteredEntities.User;
using Microsoft.Extensions.DependencyInjection;
using Domain.Entities.RegisteredEntities.Flight;

namespace Persistence
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly ProjectDBContext applicationDbContext;
		private readonly IServiceProvider serviceProvider;

		public UnitOfWork(ProjectDBContext applicationDbContext, IServiceProvider serviceProvider)
		{
			this.applicationDbContext = applicationDbContext ?? throw new ArgumentNullException(nameof(applicationDbContext));
			this.serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
		}

		public IRegisteredGenericRepository<UserEntity> UserRepository => this.serviceProvider.GetRequiredService<IRegisteredGenericRepository<UserEntity>>();
		public IRegisteredGenericRepository<FlightEntity> FlightRepository => this.serviceProvider.GetRequiredService<IRegisteredGenericRepository<FlightEntity>>();
		public IRegisteredGenericRepository<UserTokenRefreshEntity> UserTokenRefreshRepository => this.serviceProvider.GetRequiredService<IRegisteredGenericRepository<UserTokenRefreshEntity>>();

		public void Commit()
		{
			this.applicationDbContext.SaveChanges();
		}

		public Task CommitAsync(CancellationToken cancellationToken)
		{
			return this.applicationDbContext.SaveChangesAsync(cancellationToken);		
		}
	}
}
