﻿using Domain.Basics;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
	public class BaseRegisteredEntityConfiguration<TAggregate> : BaseEntityConfiguration<TAggregate> where TAggregate : BaseRegisteredEntity
	{
		public override void Configure(EntityTypeBuilder<TAggregate> builder)
		{
			base.Configure(builder);
			builder.Property(p => p.Version).IsRequired();
		}
	}
}
