﻿using Domain.Entities.RegisteredEntities.User;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations.UserConfigurations
{
	public class UserTokenRefreshConfiguration : BaseRegisteredEntityConfiguration<UserTokenRefreshEntity>
	{
		public override void Configure(EntityTypeBuilder<UserTokenRefreshEntity> builder)
		{
			base.Configure(builder);
			builder.Property(p => p.Token).IsRequired();
			builder.Property(p => p.TokenRefresh).IsRequired();
			builder.Property(p => p.Created).IsRequired();
			builder.Property(p => p.Expires).IsRequired();
		}
	}
}
