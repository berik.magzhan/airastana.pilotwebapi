﻿using Domain.Entities.RegisteredEntities.Role;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations.RoleConfigurations
{
    public class RoleConfiguration : BaseRegisteredEntityConfiguration<RoleEntity>
    {
		public override void Configure(EntityTypeBuilder<RoleEntity> builder)
		{
			base.Configure(builder);
			builder.Property(p => p.Code).IsRequired().HasMaxLength(256);
			builder.HasIndex(p => p.Code).IsUnique();
		}
	}
}
