﻿using Domain.Entities.RegisterEntities;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
	public class ProjectDBContext : DbContext
	{
		public ProjectDBContext(DbContextOptions<ProjectDBContext> options) : base(options) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProjectDBContext).Assembly);
		}

		public virtual DbSet<RegisterEvent> RegisterEvents { get; set; }
	}
}
	