﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DataTransformationObjects.Flight
{
    public class FlightListDTO : BaseListDTO
    {
        public List<FlightDTO> Items { get; set; }
    }
}
