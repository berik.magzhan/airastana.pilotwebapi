﻿namespace Shared.DataTransformationObjects.User
{
	public class UserDTO
	{
		/// <summary>
		/// Id пользователя
		/// </summary>
		public Guid UserId { get; set; }

		/// <summary>
		/// Логин пользователя
		/// </summary>
		public string UserName { get; set; }

		/// <summary>
		/// Token
		/// </summary>
		public string Token { get; set; }

		/// <summary>
		/// TokenRefresh
		/// </summary>
		public string TokenRefresh { get; set; }

		/// <summary>
		/// Список ролей
		/// </summary>
		public string Role { get; set; }
	}
}
