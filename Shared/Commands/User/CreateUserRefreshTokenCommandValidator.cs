﻿using FluentValidation;

namespace Shared.Commands.User
{
	public class CreateUserTokenRefreshCommandValidator : AbstractValidator<CreateUserTokenRefreshCommand>
	{
		public CreateUserTokenRefreshCommandValidator()
		{
			this.RuleFor(x => x.Token).NotEmpty();
			this.RuleFor(x => x.TokenRefresh).NotEmpty();
			this.RuleFor(x => x.Created).NotEmpty();
			this.RuleFor(x => x.Expires).NotEmpty();
		}
	}
}
