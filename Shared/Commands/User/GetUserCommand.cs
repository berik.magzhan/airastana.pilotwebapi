﻿using Shared.DataTransformationObjects.User;
using MediatR;

namespace Shared.Commands.User
{
	public class GetUserCommand : IRequest<UserDTO>
	{
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
