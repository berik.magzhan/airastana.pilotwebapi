﻿using MediatR;
using Shared.DataTransformationObjects.Flight;

namespace Shared.Queries.Flight
{
    public class GetFlightsQuery : IRequest<FlightListDTO>
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
    }
}
