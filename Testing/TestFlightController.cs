﻿using AirAstana.WebAPI.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace Testing
{
    public class TestFlightController
    {
        [Fact]
        public async Task TestGetItemsAsync()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var command = new Mock<Shared.Queries.Flight.GetFlightsQuery>();
            var controller = new FlightController(mediator.Object);

            //Act
            var result = await controller.GetItemsAsync(command.Object, new CancellationToken());

            //Accets
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var command = new Mock<Shared.Commands.Flight.CreateFlightCommand>();
            var controller = new FlightController(mediator.Object);

            //Act
            var result = await controller.CreateAsync(command.Object, new CancellationToken());

            //Accets
            Assert.IsType<CreatedResult>(result);
            mediator.Verify(_ => _.Send(command.Object, new CancellationToken()), Times.Exactly(1));
        }
    }
}
