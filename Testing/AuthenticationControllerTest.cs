using AirAstana.WebAPI.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace Testing
{
    public class AuthenticationControllerTest
    {
        [Fact]
        public async Task TestAuthenticationAsync()
        {
            //Arrange
            var mediator =  new Mock<IMediator>();
            var command = new Mock<Shared.Commands.User.GetUserCommand>();
            var cancelationToken = new CancellationTokenSource();
            var controller = new AuthenticationController(mediator.Object);

            //Act
            var result = await controller.AuthenticationAsync(command.Object, new CancellationToken());

            //Accets
            Assert.IsType<OkObjectResult>(result);
        }
    }
}