﻿using Domain.Basics;
using System.Linq.Expressions;

namespace Application.Abstract.Repositories
{
	public interface IRegisteredGenericRepository<TEntity> where TEntity : BaseRegisteredEntity
	{
		ValueTask<IEnumerable<TEntity>> GetAsync(
			CancellationToken cancellationToken,
			Expression<Func<TEntity, bool>>? filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
			string? includeProperties = null,
			int skip = 0,
			int take = 0);

		ValueTask<TEntity?> GetByIdAsync(Guid id, CancellationToken cancellationToken);
		IQueryable<TEntity> GetAll();
		Task<int> CountAsync(CancellationToken cancellationToken, Expression<Func<TEntity, bool>>? filter = null);
		Task<int> RaiseEvent(BaseEntityEvent @event, CancellationToken cancellationToken, bool autoCommit = true);
	}
}
