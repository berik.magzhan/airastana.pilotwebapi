﻿using Domain.Entities.RegisteredEntities.Flight;
using Domain.Entities.RegisteredEntities.User;

namespace Application.Abstract.Repositories
{
	public interface IUnitOfWork
	{
		void Commit();
		Task CommitAsync(CancellationToken cancellationToken);
		IRegisteredGenericRepository<UserEntity> UserRepository { get; }
		IRegisteredGenericRepository<UserTokenRefreshEntity> UserTokenRefreshRepository { get; }
		IRegisteredGenericRepository<FlightEntity> FlightRepository { get; }
	}
}
