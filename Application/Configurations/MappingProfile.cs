﻿using AutoMapper;
using Domain.Entities.RegisteredEntities.Flight;
using Domain.Entities.RegisteredEntities.Flight.Events;
using Shared.Commands.Flight;
using Shared.DataTransformationObjects.Flight;

namespace Application.Configurations
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateFlightMaps();
        }
        private void CreateFlightMaps()
        {
            this.CreateMap<FlightEntity, FlightDTO>();
            this.CreateMap<CreateFlightCommand, FlightCreatedEvent>();
            this.CreateMap<ChangeFlightCommand, FlightChangedEvent>()
            .ForMember(d => d.EntityId, o => o.MapFrom(s => s.Id));
        }
    }
}
