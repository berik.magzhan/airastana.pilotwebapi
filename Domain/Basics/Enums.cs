﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Basics
{
	/// <summary>
	/// Коды статусов по модели Flight (рейс)
	/// </summary>
	public enum StatusCode
	{
		/// <summary>
		/// Во время.
		/// </summary>
		InTime = 1,

		/// <summary>
		/// Задерживается. 
		/// </summary>
		Delayed = 2,

		/// <summary>
		/// Отменен.
		/// </summary>
		Cancelled = 3
	}
}
