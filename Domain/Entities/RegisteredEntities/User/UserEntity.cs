﻿using Domain.Basics;
using Domain.Entities.RegisteredEntities.Role;

namespace Domain.Entities.RegisteredEntities.User
{
	public class UserEntity : BaseRegisteredEntity
	{
		/// <summary>
		/// Логин пользователя
		/// </summary>
		public string UserName { get; private set; }

		/// <summary>
		/// Пароль пользователя
		/// </summary>
		public string Password { get; private set; }

		/// <summary>
		/// ID роля пользователя
		/// </summary>
		public Guid RoleId { get; private set; }

		/// <summary>
		/// Способ оплаты.
		/// </summary>
		public RoleEntity RoleEntity { get; private set; }
	}
}
