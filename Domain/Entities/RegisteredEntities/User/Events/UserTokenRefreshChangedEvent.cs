﻿using Domain.Basics;

namespace Domain.Entities.RegisteredEntities.User.Events
{
    public class UserTokenRefreshChangedEvent : BaseEntityEvent
	{
		/// <summary>
		/// Токен
		/// </summary>
		public string Token { get; private set; }

		/// <summary>
		/// Токен обновления
		/// </summary>
		public string TokenRefresh { get; set; }

		/// <summary>
		/// Дата и время создания
		/// </summary>
		public DateTimeOffset Created { get; set; }

		/// <summary>
		/// Дата и время действия
		/// </summary>
		public DateTimeOffset Expires { get; set; }
	}
}
