﻿using Domain.Basics;
using Domain.Entities.RegisteredEntities.User.Events;

namespace Domain.Entities.RegisteredEntities.User
{
    public class UserTokenRefreshEntity : BaseRegisteredEntity
	{
		/// <summary>
		/// Токен
		/// </summary>
		public string Token { get; private set; }

		/// <summary>
		/// Токен обновления
		/// </summary>
		public string TokenRefresh { get; private set; }

		/// <summary>
		/// Дата и время создания
		/// </summary>
		public DateTimeOffset Created { get; private set; }

		/// <summary>
		/// Дата и время действия
		/// </summary>
		public DateTimeOffset Expires { get; private set; }

		#region

		public void Apply(UserTokenRefreshCreatedEvent @event)
		{
			this.Id = Guid.NewGuid();
			@event.EntityId = this.Id;
			this.Token = @event.Token;
			this.TokenRefresh = @event.TokenRefresh;
			this.Created = @event.Created;
			this.Expires = @event.Expires;
		}

		public void Apply(UserTokenRefreshChangedEvent @event)
		{
			this.Token = @event.Token;
			this.TokenRefresh = @event.TokenRefresh;
			this.Created = @event.Created;
			this.Expires = @event.Expires;
		}

		#endregion
	}
}
