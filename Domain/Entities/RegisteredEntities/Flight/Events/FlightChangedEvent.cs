﻿using Domain.Basics;

namespace Domain.Entities.RegisteredEntities.Flight.Events
{
	public class FlightChangedEvent : BaseEntityEvent
	{
		/// <summary>
		/// Статус.
		/// </summary>
		public StatusCode Status { get; set; }
	}
}
