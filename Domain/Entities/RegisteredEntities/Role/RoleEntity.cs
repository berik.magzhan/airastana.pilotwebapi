﻿using Domain.Basics;

namespace Domain.Entities.RegisteredEntities.Role
{
	public class RoleEntity : BaseRegisteredEntity
	{
		/// <summary>
		/// Код роля
		/// </summary>
		public string Code { get; set; }
	}
}
