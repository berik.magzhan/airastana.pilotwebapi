﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.RegisterEntities
{
	/// <summary>
	/// Для фиксации и аудита собитии в системе
	/// </summary>
	public class RegisterEvent
    {
		public uint Id { get; set; }
		public Guid EntityId { get; set; }
		public DateTime EntityCreated { get; set; }
		public DateTime EventDateTime { get; set; }
		public int Version { get; set; }
		public string EventName { get; set; }
		public string EventJson { get; set; }
	}
}
