﻿using Logger.Extensions;
using Logger.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading.Tasks;

namespace Logger.MiddleWare
{
    public class IdempotencyLoggingMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<IdempotencyLoggingMiddleware> logger;
        private readonly IdempotencyLoggingOptions options;

        public IdempotencyLoggingMiddleware(RequestDelegate next, ILogger<IdempotencyLoggingMiddleware> logger, IOptions<IdempotencyLoggingOptions> options)
        {
            this.next = next ?? throw new ArgumentNullException(nameof(next));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.options = options.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            if (httpContext is null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            if (httpContext.Request.Headers.TryGetValue(options.IdempotencyHeader, out StringValues idempotencyKeyValue))
            {
                using (logger.BeginScopeWith((options.IdempotencyLogAttribute, idempotencyKeyValue.ToString())))
                {
                    await this.next(httpContext);
                }
            }
            else
            {
                await this.next(httpContext);
            }
        }
    }
}
